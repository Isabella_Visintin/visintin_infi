
import java.sql.*;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.*;


public class DBManager {
	private String pw = "Sarah3618";
	private String dbName = "Temperaturmessung";
	private Scanner sc = new Scanner(System.in);
	private String sql = "CREATE TABLE IF NOT EXISTS Temperaturmessung (temperature INT PRIMARY KEY, date DATE NOT NULL)";
	
	 DBManager() throws SQLException {
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
	}

	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:mysql://localhost/" + dbName, "root", pw);	
	}
	
	public void createTable() throws SQLException {
		Statement stmt = null;
		
		Connection c = getConnection();
		stmt = c.createStatement();
		try {
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public char menu() throws SQLException {

		while (true) {
			System.out.println("Was wollen Sie in der Datenbank tun?");
			System.out.println("e ... Temperaturen einf�gen");
			System.out.println("a ... Temperaturen auslesen");
			String answer = sc.next();
			switch (answer) {
			case "e":
				insertTem();
				break;
			case "a":
				showTem();
				break;
			default:
				System.out.println("Bitte geben Sie einen anderen Buchstaben ein");
				break;
			}

		}
	}

	
	public void insertInto(Temperature tem) throws SQLException {
		Connection c = getConnection();
		String sql = "INSERT INTO Temperaturmessung (Temperatur, Datum) VALUES (?,?)";
		PreparedStatement stmt = null;

		try {
			stmt = c.prepareStatement(sql);
			stmt.setInt(1, tem.getTemperature());
			stmt.setString(2, tem.getDateStr());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					if (stmt != null) {
						stmt.close();
					}

					if (c != null) {
						c.close();
					}
				}
			}
		}

	}


	public void insertTem() throws SQLException {
		String temDate = "";
		int tem = 0;
		boolean errDate = false;
		boolean errTem = false;
		do {
			System.out.println("Bitte Temperatur eingeben: ");
			try {
				errTem = false;
				tem = Integer.parseInt(sc.next());
			} catch (Exception e) {
				System.out.println("Falsche Eingabe");
				errTem = true;
			}
		} while (errTem);
		do {
			System.out.println("Datum angeben?: [j/n]");
			switch (sc.next()) {
			case "j":
				System.out.println("Bitte Datum eingeben");
				System.out.println("Jahr: ");
				String Year = sc.next();
				System.out.println("Monat");
				String Month = sc.next();
				System.out.println("Tag");
				String Day = sc.next();


				temDate = Year + "-" + Month + "-" + Day;
				System.out.println(temDate);
				errDate = false;
				break;
			case "n":
				Date date = new Date(tem);
				SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
				temDate = sdf.format(date);
				System.out.println(temDate);
				errDate = false;
				break;
			default:
				System.out.println("Sie haben ein falsches Datum eingegeben");
				errDate = true;
				break;
			}
		} while (errDate);

		Temperature t = new Temperature(tem, temDate);
		insertInto(t);
	}
	
	public ArrayList<Temperature> select() throws SQLException {
		Connection c = getConnection();
		String sql = "SELECT * FROM temperaturmessung";
		PreparedStatement stmt = null;
		ArrayList<Temperature> list = new ArrayList<Temperature>();

		try {
			
			stmt = c.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				list.add(new Temperature(rs.getInt(2), rs.getString(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (c != null) {
				c.close();
			}
		}
		return list;
	}

	public void showTem() throws SQLException {
		ArrayList<Temperature> list = new ArrayList<Temperature>();
		list = select();
		for (int i =0; i < list.size();i++) {
			System.out.println("Temperatur: " + list.get(i).getTemperature());
			System.out.println("Datum: " + list.get(i).getDateStr());
			System.out.println();
		}

	}

	

}

	
	
	

